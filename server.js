const express = require('express')
const cors = require('cors')
const port = process.env.PORT || 8030
const app = express()

app.use(cors())

//Static
app.use(express.static(__dirname + '/dist/'))

//Output
app.listen(port, () => {
  console.log('Server Vue iniciado en ' + port)
})
